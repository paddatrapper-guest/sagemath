#!/usr/bin/python3
# -*- coding: utf-8 -*-

import os
import codecs
import subprocess
import sys
import time
import imp

system_installed_packages = 0
system_packages_not_found = 0
installed_dir = '@SAGE_ROOT@/local/var/lib/sage/installed'
now = int(time.time())

def mkdir_if_needed(dirname):
    if not os.path.exists(dirname):
        os.makedirs(dirname)

def declare_system(package):
    global system_installed_packages
    version = subprocess.check_output(['/bin/cat', '@SAGE_ROOT@/build/pkgs/{0:s}/package-version.txt'.format(package)]).decode().strip()
    filename = '{0:s}/{1:s}-{2:s}'.format(installed_dir, package, version)
    installed = codecs.open(filename, 'w')
    installed.write ('system')
    installed.close()
    # This is ABSOLUTELY NECESSARY: otherwise sage's Makefiles will think that
    # pkgs whose "package-version.txt" files that we touched later (simply
    # because we listed them later in "dico" below), obsolete their
    # reverse-dependencies further back in the list, and sage will try to
    # download and build these destroying all our hard work here.
    #
    # The "cleaner" way to fix this is to re-order dico below to be in reverse
    # topological order wrt the dependency graph, but I couldn't be bothered
    # figuring out what that is.
    os.utime(filename, (now, now))
    system_installed_packages += 1


mkdir_if_needed (installed_dir)
mkdir_if_needed ('@SAGE_ROOT@/local')
mkdir_if_needed ('@SAGE_ROOT@/local/include')
# if this file is newer than an spkg's "installed" marker file then sage's
# Makefiles will try to rebuild it again, so touch it as well.
dummy_pkg = os.path.join(installed_dir, ".dummy")
with open(dummy_pkg, 'a'):
    os.utime(dummy_pkg, (now, now))

sage_standard_packages = set(subprocess.check_output([
    '/bin/sh', '-c',
    'for p in @SAGE_ROOT@/build/pkgs/*; do test $(cat $p/type) = standard && basename $p; done'
]).split())

dico = {
    'appnope': @HAS_APPNOPE@,
    'arb': @HAS_ARB@,
    'boost_cropped': @HAS_BOOST@,
    'brial': @HAS_POLYBORI@,
    'bzip2': @HAS_BZIP2@,
    'cddlib': @HAS_CDDLIB@,
    'certifi': @HAS_CERTIFI@,
    'cliquer': @HAS_CLIQUER@,
    'combinatorial_designs': @HAS_COMBINATORIAL_DESIGNS@,
    'conway_polynomials': @HAS_CONWAY_POLYNOMIALS@,
    'curl': @HAS_CURL@,
    'cython': @HAS_CYTHON@,
    'ecl': @HAS_ECL@,
    'eclib': @HAS_ECLIB@,
    'ecm': @HAS_ECM@,
    'elliptic_curves': @HAS_ELLIPTIC_CURVES@,
    'fflas_ffpack': @HAS_FFLAS_FFPACK@,
    'flint': @HAS_FLINT@,
    'flintqs': @HAS_FLINTQS@,
    'fplll': @HAS_FPLLL@,
    'freetype': @HAS_FREETYPE@,
    'gap': @HAS_GAP@,
    'gc': @HAS_BOEHM_GC@,
    'libbraiding': @HAS_LIBBRAIDING@,
    'libgd': @HAS_GD@,
    'libhomfly': @HAS_LIBHOMFLY@,
    'gf2x': @HAS_GF2X@,
    'gfan': @HAS_GFAN@,
    'gcc': @HAS_GFORTRAN@,
    'giac': @HAS_GIAC@,
    'git': @HAS_GIT@,
    'givaro': @HAS_GIVARO@,
    'glpk': @HAS_GLPK@,
    'graphs': @HAS_GRAPHS@,
    'gsl': @HAS_GSL@,
    'iconv': @HAS_ICONV@,
    'iml': @HAS_IML@,
    'ipython': @HAS_IPYTHON@,
    'jmol': @HAS_JMOL@,
    'jupyter_client': @HAS_JUPYTER_CLIENT@,
    'jupyter_core': @HAS_JUPYTER_CORE@,
    'lcalc': @HAS_LCALC@,
    'libpng': @HAS_LIBPNG@,
    'linbox': @HAS_LINBOX@,
    'lrcalc': @HAS_LRCALC@,
    'm4ri': @HAS_M4RI@,
    'm4rie': @HAS_M4RIE@,
    'markupsafe': @HAS_MARKUPSAFE@,
    'mathjax': @HAS_MATHJAX@,
    'maxima': @HAS_MAXIMA@,
    'mpc': @HAS_MPC@,
    'mpfi': @HAS_MPFI@,
    'mpfr': @HAS_MPFR@,
    'mpir': @HAS_MPIR@,
    'nauty': @HAS_NAUTY@,
    'ncurses': @HAS_NCURSES@,
    'ntl': @HAS_NTL@,
    'openblas': @HAS_BLAS@,
    'palp': @HAS_PALP@,
    'pari': @HAS_PARI@,
    'pari_galdata': @HAS_PARI_GALDATA@,
    'pari_seadata_small': @HAS_PARI_SEADATA@,
    'patch': @HAS_PATCH@,
    'pcre': @HAS_PCRE@,
    'pip': @HAS_PIP@,
    'pkgconf': @HAS_PKGCONF@,
    'pkgconfig': @HAS_PKGCONFIG@,
    'planarity': @HAS_PLANARITY@,
    'polytopes_db': @HAS_POLYTOPES@,
    'ppl': @HAS_PPL@,
    'pynac': @HAS_PYNAC@,
    'python2': @HAS_PYTHON@,
    'python3': @HAS_PYTHON3@,
    'r': @HAS_R@,
    'ratpoints': @HAS_RATPOINTS@,
    # those two go together afaik:
    'readline': @HAS_READLINE@,
    'termcap': @HAS_READLINE@,
    'rubiks': @HAS_RUBIKS@,
    'rw': @HAS_RW@,
    'sagetex': True, # to break a circular build-dependency, and it's not actually needed by the build
    'scons': @HAS_SCONS@,
    'singular': @HAS_SINGULAR@,
    'sqlite': @HAS_SQLITE@,
    'symmetrica': @HAS_SYMMETRICA@,
    'sympow': @HAS_SYMPOW@,
    'tachyon': @HAS_TACHYON@,
    'thebe': @HAS_THEBE@,
    'threejs': @HAS_THREEJS@,
    'widgetsnbextension': @HAS_WIDGETSNBEXTENSION@,
    'xz': @HAS_XZ@,
    'yasm': @HAS_YASM@,
    'zeromq': @HAS_ZEROMQ@,
    'zlib': @HAS_ZLIB@,
    'zn_poly': @HAS_ZNPOLY@
}

python_modules = [
    'alabaster',
    'babel',
    'backports_abc',
    'configparser',
    'cvxopt',
    'cycler',
    'cysignals',
    'dateutil',
    'decorator',
    'docutils',
    'entrypoints',
    'flask',
    'flask_autoindex',
    'flask_babel',
    'flask_oldsessions',
    'flask_openid',
    'flask_silk',
    'fpylll',
    'functools32',
    'future',
    'imagesize',
    'ipykernel',
    'ipython_genutils',
    'ipywidgets',
    'itsdangerous',
    'jinja2',
    'jsonschema',
    'matplotlib',
    'mistune',
    'mpmath',
    'nbconvert',
    'nbformat',
    'networkx',
    'notebook',
    'numpy',
    'packaging',
    'pathlib2',
    'pexpect',
    'pickleshare',
    'prompt_toolkit',
    'psutil',
    'ptyprocess',
    'pygments',
    'pyparsing',
    'pytz',
    'rpy2',
    'sagenb',
    'sagenb_export',
    'scipy',
    'setuptools',
    'setuptools_scm',
    'simplegeneric',
    'singledispatch',
    'six',
    'snowballstemmer',
    'sphinx',
    'sympy',
    'terminado',
    'tornado',
    'traitlets',
    'twisted',
    'typing',
    'vcversioner',
    'wcwidth',
    'werkzeug'
]

python_packages_to_modules = {
    'backports_shutil_get_terminal_size': 'backports/shutil_get_terminal_size',
    'cypari': 'cypari2',
    'enum34': 'enum',
    'pathpy': 'path',
    'pillow': 'PIL',
    'python_openid': 'openid',
    'pyzmq': 'zmq',
    'zope_interface': 'zope/interface'
}

unneeded_packages = {
    'backports_functools_lru_cache',
    'backports_ssl_match_hostname',
    'bleach',
    'html5lib',
    'ipaddress',
    'kiwisolver', # dependency of matplotlib
    'libatomic_ops',
    'pandocfilters',
    'prometheus_client',
    'pycygwin',
    'requests', # FIXME(requests): only used to build sphinx, not needed in Debian
    'scandir',
    'send2trash',
    'speaklater',
    'sphinxcontrib_websupport',
    'subprocess32',
    'testpath',
    'webencodings'
}

for p in python_modules:
    try:
        imp.find_module(p)
        dico[p] = True
    except ImportError:
        dico[p] = False

for p, m in python_packages_to_modules.iteritems():
    try:
        imp.find_module(m)
        dico[p] = True
    except ImportError:
        dico[p] = False

for p in unneeded_packages:
    dico[p] = True


# report missing standard packages too
for p in sage_standard_packages:
    if p not in dico:
        dico[p] = False

for key, val in sorted(dico.items()):
    if val:
        declare_system(key)
    else:
        system_packages_not_found += 1
        print ('{0:s} will not come from debian!'.format (key))

print('{0:d} system packages will be used'.format(system_installed_packages))
sys.exit(system_packages_not_found)
